import { sveltekit } from '@sveltejs/kit/vite';
import { SvelteKitPWA } from '@vite-pwa/sveltekit';
import { FSWatcher } from 'vite';
import { defineConfig } from 'vitest/config';

//@ts-ignore
import { readFileSync } from 'fs';

// For netlify build environment
let httpsConf = {};
if (process.env.VITE_BUILD_ENV === 'netlify') {
	httpsConf = {};
} else {
	httpsConf = {
		key: readFileSync('/config/workspace/ssl/selfsign.key'),
		cert: readFileSync('/config/workspace/ssl/selfsign.pem')
	};
}

export default defineConfig({
	plugins: [
		sveltekit(),
		SvelteKitPWA({
			srcDir: './src',
			// you don't need to do this if you're using generateSW strategy in your app
			//strategies: generateSW ? 'generateSW' : 'injectManifest',
			// you don't need to do this if you're using generateSW strategy in your app
			//filename: generateSW ? undefined : 'prompt-sw.ts',
			scope: '/',
			base: '/',
			selfDestroying: process.env.SELF_DESTROYING_SW === 'true',
			pwaAssets: {
				config: true
			},
			manifest: {
				short_name: '设备定位',
				name: '设备定位 PWA',
				lang: 'zh-CN',
				start_url: '/',
				scope: '/',
				display: 'standalone',
				// TODO： edit theme color
				theme_color: '#000000',
				background_color: '#000000'
			},
			injectManifest: {
				globPatterns: ['client/**/*.{js,css,ico,png,svg,webp,woff,woff2}']
			},
			workbox: {
				globPatterns: ['client/**/*.{js,css,ico,png,svg,webp,woff,woff2,csv}']
			},
			devOptions: {
				enabled: false,
				suppressWarnings: process.env.SUPPRESS_WARNING === 'true',
				type: 'module',
				navigateFallback: '/'
			},
			// if you have shared info in svelte config file put in a separate module and use it also here
			kit: {
				includeVersionFile: true
			}
		})
	],
	test: {
		include: ['src/**/*.{test,spec}.{js,ts}']
	},
	server: {
		host: '0.0.0.0',
		port: 3000,
		https: httpsConf
	},
	preview: {
		host: '0.0.0.0',
		port: 3000,
		https: httpsConf
	},
	define: {
		__APP_VERSION__: JSON.stringify(process.env.npm_package_version)
	}
});
