import { parse } from 'csv-parse/browser/esm/sync';
import { fetchWithTimeout } from './utils-async';

type DevObj = {
	id: number;
	code9: string;
	comment: string;
	plantZhName: string;
	devZhName: string;
};

class CodeDB {
	csvlink: string;
	data: Array<DevObj>;

	/**
	 * Init code db
	 * @param csvlink link to csv file.
	 * @example
	 * ```
	 * cdb = new CodeDB('/1.csv')
	 * await cdb.load()
	 * ```
	 */
	constructor(csvlink: string) {
		this.csvlink = csvlink;
		this.data = [];
	}

	/**
	 * Load csv file
	 */
	async load() {
		let r: Response;
		r = await fetchWithTimeout(this.csvlink);
		if (!r.ok) {
			throw new Error(`Network Error: ${r.status}`);
		}
		const csvOption = { columns: false, from_line: 2, skip_empty_lines: true };
		const csvData = parse(await r.text(), csvOption);

		// Skip update same data
		if (csvData.length === this.data.length) {
			return;
		}

		//console.log(csvData);
		for (const i of csvData) {
			let obj: DevObj = {
				id: i[0],
				code9: i[1],
				comment: i[2],
				plantZhName: i[3],
				devZhName: i[4]
			};
			this.data.push(obj);
			//this.data = [...this.data, obj];
		}
		// Sort data.comment ascending
		this.data.sort((a, b) => {
			const x = a.comment.toLowerCase();
			const y = b.comment.toLowerCase();
			if (x > y) {
				return 1;
			} else if (x < y) {
				return -1;
			} else {
				return 0;
			}
		});
		//console.log(this.data);
	}
	search(searchText: string) {
		searchText = searchText.toUpperCase();

		// search with exact match, prefix match and any match
		const exactResult = this.data.filter((page) => {
			return page.code9 === searchText;
		});

		const prefixResult = this.data.filter((page) => {
			return page.code9.startsWith(searchText);
		});

		// Search anything in single line
		const anyResult = this.data.filter((page) => {
			return JSON.stringify(page).includes(searchText);
		});

		// merge results
		let result = [...new Set([...exactResult, ...prefixResult, ...anyResult])];
		return result;
	}
}

export type { DevObj };
export { CodeDB };
