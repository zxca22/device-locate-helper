/* Some useful function for async */

/**
 * Async timeout
 * Ref: https://stackoverflow.com/questions/33289726
 * @param {number} ms timeout
 */
function timeout(ms) {
	return new Promise((resolve) => setTimeout(resolve, ms));
}

/**
 * Timeout fetch
 * Ref: https://dmitripavlutin.com/timeout-fetch-request/
 * @param {*} resource
 * @param {*} options
 */

async function fetchWithTimeout(resource, options = {}) {
	const { timeout = 5000 } = options;

	const controller = new AbortController();
	const id = setTimeout(() => controller.abort(), timeout);

	const response = await fetch(resource, {
		...options,
		signal: controller.signal
	});
	clearTimeout(id);

	return response;
}

export { timeout, fetchWithTimeout };
