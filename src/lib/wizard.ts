import type { CodeDB, DevObj } from './code-db';
import Tesseract from 'tesseract.js';
import { tsocrProgress } from './stores';

type searchResultsObj = {
	id: number;
	searchText: string;
	result: DevObj[];
};

const code9RegExp = /(\w{6,7}\/\w{3,5})|([A-Z0-9]{4,10})/g;

function replaceStrByIndex(str: string, index: number, char: string) {
	const strArr = str.split('');
	strArr[index] = char;
	return strArr.join('');
}

async function get9CodeArray(text: string) {
	const matchResult = text.toUpperCase().matchAll(code9RegExp);
	let result = [];
	for (const i of matchResult) {
		let text = i[0];
		// Fixme: OCR Fix
		text = text.replace(/^O/, '0');
		text = text.replace(/0{1,2}$/, 'O');

		if (text.length === 9) {
			for (let i = 4; i < 9; i++) {
				if (text[i] === 'O' && i < 6) {
					text = replaceStrByIndex(text, i, '0');
				} else if (text[i] === '0' && i > 6) {
					text = replaceStrByIndex(text, i, 'O');
				}
			}
		}
		result.push(text);
	}
	return result;
}

async function searchCdbByArray(codeArray: string[], cdbInstance: CodeDB) {
	let result: searchResultsObj[] = [];
	for (let index = 0; index < codeArray.length; index++) {
		const sText = codeArray[index];
		result.push({
			id: index,
			searchText: sText,
			result: cdbInstance.search(sText)
		});
	}
	return result;
}

async function getErrorResult(input: searchResultsObj[]) {
	let result: searchResultsObj[] = [];
	for (const i of input) {
		if (i.result.length != 1) {
			result.push(i);
		}
	}
	return result;
}

class TsOCR {
	worker: Tesseract.Worker | undefined = undefined;
	constructor() {}
	async init() {
		/* 	this.worker = await Tesseract.createWorker('eng', 1, {
			workerPath: '../node_modules/tesseract.js/dist/worker.min.js',
			langPath: '../lang-data',
			corePath: '../node_modules/tesseract.js-core/tesseract-core.wasm.js',
			logger: (m) => console.log(m)
		}); */
		this.worker = await Tesseract.createWorker('eng', 1, {
			logger: (m) => {
				console.log(m);
				tsocrProgress.set(m.progress);
			}
		});
		await this.worker.setParameters({
			user_defined_dpi: '72'
		});
	}
	async recognize(img: File, opt: Partial<Tesseract.RecognizeOptions> = {}) {
		if (this.worker) {
			const r = await this.worker.recognize(img, opt);
			return r.data.text;
		} else {
			throw new Error('Tesseract.js Worker not init!');
		}
	}
}

export type { searchResultsObj };
export { get9CodeArray, searchCdbByArray, getErrorResult, TsOCR };
