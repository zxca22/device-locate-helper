import { CodeDB } from './code-db';
import { writable } from 'svelte/store';

const cdb = writable(new CodeDB('empty'));

const loading = writable(false);

const tsocrProgress = writable(0);

export { cdb, loading ,tsocrProgress};
