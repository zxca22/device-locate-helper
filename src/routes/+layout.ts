// SSG
// ref: https://github.com/sveltejs/kit/discussions/3365#discussioncomment-1976132
export const prerender = true;
export const ssr = false;
export const trailingSlash = 'never';
